// ignore_for_file: unnecessary_new

import 'package:flutter/material.dart';
import 'package:lab_7/widgets/main_drawer.dart';

class profile extends StatefulWidget {
  @override
  _profile createState() => _profile();
}

class _profile extends State<profile> {
  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar( title: const Text(
        'Covid Consult',
        style: TextStyle(
          color: Color.fromRGBO(255, 255, 255, 1)
        ),
      ),
        backgroundColor: const Color.fromRGBO(20, 20, 20, 1),
      ),
      drawer: MainDrawer(),
      body: Center(
        child: 
        Column(children: [
          Container(
            margin: const EdgeInsets.only(top: 30),
            child: const Text(
              "Profile",
              style: TextStyle(
                fontSize: 30,
                fontFamily: 'Mulish',
                fontWeight: FontWeight.bold,
                color: Color.fromRGBO(255, 255, 255, 1),
              ),
            )
          ),
          Container(
            height: 250,
            // width: 375,
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(top: 20, bottom: 10, left: 10, right: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: const Color.fromRGBO(20, 20, 20, 1),
              boxShadow: const [
                BoxShadow(
                  color: Color.fromRGBO(158, 67, 115, 1),
                  spreadRadius: 4,
                  blurRadius: 4,
                  offset: Offset(3, 3),
                )
              ],
            ),
            child: Column(children: [
              const Text(
                "User Data",
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Mulish',
                  color: Colors.white
                ),
              ),
              SizedBox(height: 10),
              Row(children: const [
                Text("Name" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
                SizedBox(width: 80,),
                Text("Pramudiptha" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
              ],),
              SizedBox(height: 10),
              Row(children: const [
                Text("Gender" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
                SizedBox(width: 72,),
                Text("Male" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
              ],),
              SizedBox(height: 10),
              Row(children: const [
                Text("Birth Date" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
                SizedBox(width: 53,),
                Text("Nov 11, 2001" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
              ],),
              SizedBox(height: 10),
              Row(children: const [
                Text("E-Mail" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
                SizedBox(width: 78,),
                Text("pramudiptha@ui.ac.id" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
              ],),
              SizedBox(height: 10),
              Row(children: const [
                Text("Phone Number" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
                SizedBox(width: 20,),
                Text("081295759817" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
              ],),
              SizedBox(height: 10),
              OutlineButton(onPressed: () {_showFullModal(context, _formKey);},
              child: const Text("Edit", style: TextStyle(fontSize: 15, fontFamily: 'Mulish', fontWeight: FontWeight.bold, color: Colors.white),),
              highlightedBorderColor: const Color.fromRGBO(233, 216, 253, 1),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
              borderSide: const BorderSide(
                width: 1,
                color: Color.fromRGBO(233, 216, 253, 1),
                style: BorderStyle.solid,
              ),
              )
            ],),
          ),
          Container(
            height: 165,
            width: 375,
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(top: 20, bottom: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: const Color.fromRGBO(20, 20, 20, 1),
              boxShadow: const [
                BoxShadow(
                  color: Color.fromRGBO(158, 67, 115, 1),
                  spreadRadius: 4,
                  blurRadius: 4,
                  offset: Offset(3, 3),
                )
              ],
            ),
            child: Column(children: [
              const Text(
                "Account Data",
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Mulish',
                  color: Colors.white
                ),
              ),
              SizedBox(height: 10),
              Row(children: const [
                Text("Username" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
                SizedBox(width: 48,),
                Text("Pramudiptha" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
              ],),
              SizedBox(height: 10),
              Row(children: const [
                Text("User Type" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
                SizedBox(width: 50,),
                Text("Male" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
              ],),
              SizedBox(height: 10),
              OutlineButton(onPressed: () {},
              child: const Text("Change Password", style: TextStyle(fontSize: 15, fontFamily: 'Mulish', fontWeight: FontWeight.bold, color: Colors.white),),
              highlightedBorderColor: const Color.fromRGBO(233, 216, 253, 1),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
              borderSide: const BorderSide(
                width: 1,
                color: Color.fromRGBO(233, 216, 253, 1),
                style: BorderStyle.solid,
              ),
              )
            ],),
          )
        ],)
      ),
    );
  }
}

_showFullModal(context, formkey) {
    showGeneralDialog(
      context: context,
      barrierDismissible: false, // should dialog be dismissed when tapped outside
      barrierLabel: "Modal", // label for barrier
      transitionDuration: Duration(milliseconds: 250), // how long it takes to popup dialog after button click
      pageBuilder: (_, __, ___) { // your widget implementation 
        return Scaffold(
          appBar: AppBar(
            backgroundColor: const Color.fromRGBO(20, 20, 20, 1),
            centerTitle: true,
            leading: IconButton(
              icon: const Icon(
                Icons.close,
                color: Colors.white,
              ), 
              onPressed: (){
                Navigator.pop(context);
              }
            ),
            title: const Text("Change Data" , style: TextStyle(fontFamily: 'Mulish', fontSize: 30, fontWeight: FontWeight.bold),),
            elevation: 0.0
          ),
          backgroundColor: Colors.white,
          body: Container(
            padding: const EdgeInsets.fromLTRB(20, 10, 20, 10),
            decoration: const BoxDecoration(
              border: Border(
                top: BorderSide(
                  color: Color(0xfff8f8f8),
                  width: 1,
                ),
              ),
            ),
            child: Form(
              key: formkey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                const SizedBox(height: 10),
                TextFormField(
                  decoration: InputDecoration(
                  icon: const Icon(Icons.person),
                  hintText: "Insert full name here ...",
                  labelText: "Name",
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
                  ),
                ), 
                const SizedBox(height: 10),
                TextFormField(
                  decoration: InputDecoration(
                  icon: const Icon(Icons.calendar_today),
                  hintText: "Insert birth date here ...",
                  labelText: "Birth Date",
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
                  ),
                ), 
                const SizedBox(height: 10),
                TextFormField(
                  keyboardType: TextInputType.emailAddress,
                  decoration: InputDecoration(
                  icon: const Icon(Icons.mail),
                  hintText: "Insert e-mail here ...",
                  labelText: "E-Mail",
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
                  ),
                ), 
                const SizedBox(height: 10),
                TextFormField(
                  keyboardType: TextInputType.phone,
                  decoration: InputDecoration(
                  icon: const Icon(Icons.phone),
                  hintText: "Insert phone number here ...",
                  labelText: "Phone Number",
                  border: OutlineInputBorder(borderRadius: BorderRadius.circular(5.0)),
                  ),
                ),
                const SizedBox(height: 20),
                Row(mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: 100.0,
                      height: 50.0,
                      child: 
                      OutlinedButton(onPressed: () {Navigator.pop(context);},
                        style: OutlinedButton.styleFrom(
                          primary: Colors.white,
                          backgroundColor: Theme.of(context).primaryColor,
                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
                          // side: const BorderSide(color: Color.fromRGBO(233, 216, 253, 1), width: 2.5)
                        ),
                      child: const Text("Submit", style: TextStyle(fontSize: 20, fontFamily: 'Mulish', fontWeight: FontWeight.w500, color: Colors.white),),
                      )
                    )
                  ],
                )
                ],
              ),
            ),
          )
        );
      },
    );
  }
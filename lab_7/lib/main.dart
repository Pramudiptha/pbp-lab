import 'package:flutter/material.dart';
import 'package:lab_7/screens/profilepage.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp (
      title: 'Covid Consult',
      home: profile(),
      theme: ThemeData (
        fontFamily: 'Mulish',
        primaryColor: const Color.fromRGBO(56, 56, 56, 1),
      ),
    );
  }
}

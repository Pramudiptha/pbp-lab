1. Apakah perbedaan antara JSON dan XML?
- JSON merupakan format yang ditulis dalam JavaScript, sedangkan XML merupakan bahasa markup
- JSON lebih cepat karena datanya diproses secara serial, sedangkan XML lebih lambat karena banyaknya bandwith
- JSON menyimpan data sebagai map, sedangkan XML menyimpan data sebagai tree structure
- JSON memiliki ukuran file yang sederhana, sedangkan XML memiliki ukuran file yang lebih besar
- JSON memiliki syntax yang lebih mudah untuk dipahami, sedangkan XML memiliki syntax yang lebih rumit

2. Apakah perbedaan antara HTML dan XML?
- HTML lebih fokus untuk penyajian data, sedangkan XML lebih fokus untuk transfer data
- HTML itu tidak case sensitive atau case insensitive, sedangkan XML itu case sensitive
- HTML memiliki tag terbatas, sedangkan tag XML dapat dikembangkan
- HTML tidak menyediakan dukungan namespace, sedangkan XML menyediakan dukungan namespace
- HTML didorong format, sedangkan XML didorong konten 
import 'package:flutter/material.dart';

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon, void Function() tapHandler) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: const TextStyle(
          fontFamily: 'Mulish',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
      onTap: tapHandler,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
          // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          Container(
            height: 120,
            width: double.infinity,
            padding: EdgeInsets.all(20),
            alignment: Alignment.centerLeft,
            color: const Color.fromRGBO(27, 27, 27, 1),
            child: const Text(
              'Covid Consult',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 30,
                  color: Color.fromRGBO(255, 255, 255, 1),)
            ),
          ),
          SizedBox(
            height: 20,
          ),
          buildListTile('Home', Icons.home, () {
            Navigator.pop(context);
          }),
          buildListTile('Consultation', Icons.event, () {
            Navigator.pop(context);
          }),
          buildListTile('Forum', Icons.forum, () {
            Navigator.pop(context);
          }),
          buildListTile('ObatPedia', Icons.medication, () {
            Navigator.pop(context);
          }),
          buildListTile('Article', Icons.article, () {
            Navigator.pop(context);
          }),
          buildListTile('Login', Icons.login, () {
            Navigator.pop(context);
          }),
        ],
      ),
    );
  }
}
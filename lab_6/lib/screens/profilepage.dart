import 'package:flutter/material.dart';
import 'package:lab_6/widgets/main_drawer.dart';

class profile extends StatelessWidget {
  const profile({ Key? key }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar( title: const Text(
        'Covid Consult',
        style: TextStyle(
          color: Color.fromRGBO(255, 255, 255, 1)
        ),
      ),
        backgroundColor: const Color.fromRGBO(20, 20, 20, 1),
      ),
      drawer: MainDrawer(),
      body: Center(
        child: 
        Column(children: [
          Container(
            margin: const EdgeInsets.only(top: 30),
            child: const Text(
              "Profile",
              style: TextStyle(
                fontSize: 30,
                fontFamily: 'Mulish',
                fontWeight: FontWeight.bold,
                color: Color.fromRGBO(255, 255, 255, 1),
              ),
            )
          ),
          Container(
            height: 250,
            width: 375,
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(top: 20, bottom: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: const Color.fromRGBO(20, 20, 20, 1),
              boxShadow: const [
                BoxShadow(
                  color: Color.fromRGBO(158, 67, 115, 1),
                  spreadRadius: 4,
                  blurRadius: 4,
                  offset: Offset(3, 3),
                )
              ],
            ),
            child: Column(children: [
              const Text(
                "User Data",
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Mulish',
                  color: Colors.white
                ),
              ),
              SizedBox(height: 10),
              Row(children: const [
                Text("Name" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
                SizedBox(width: 80,),
                Text("Pramudiptha" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
              ],),
              SizedBox(height: 10),
              Row(children: const [
                Text("Gender" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
                SizedBox(width: 72,),
                Text("Male" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
              ],),
              SizedBox(height: 10),
              Row(children: const [
                Text("Birth Date" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
                SizedBox(width: 53,),
                Text("Nov 11, 2001" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
              ],),
              SizedBox(height: 10),
              Row(children: const [
                Text("E-Mail" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
                SizedBox(width: 78,),
                Text("pramudiptha@ui.ac.id" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
              ],),
              SizedBox(height: 10),
              Row(children: const [
                Text("Phone Number" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
                SizedBox(width: 20,),
                Text("081295759817" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
              ],),
              SizedBox(height: 10),
              OutlineButton(onPressed: () {},
              child: const Text("Edit", style: TextStyle(fontSize: 15, fontFamily: 'Mulish', fontWeight: FontWeight.bold, color: Colors.white),),
              highlightedBorderColor: const Color(0xE9D8FD),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
              borderSide: const BorderSide(
                width: 1,
                color: Color.fromRGBO(233, 216, 253, 1),
                style: BorderStyle.solid,
              ),
              )
            ],),
          ),
          Container(
            height: 165,
            width: 375,
            alignment: Alignment.centerLeft,
            padding: const EdgeInsets.all(10),
            margin: const EdgeInsets.only(top: 20, bottom: 10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20),
              color: const Color.fromRGBO(20, 20, 20, 1),
              boxShadow: const [
                BoxShadow(
                  color: Color.fromRGBO(158, 67, 115, 1),
                  spreadRadius: 4,
                  blurRadius: 4,
                  offset: Offset(3, 3),
                )
              ],
            ),
            child: Column(children: [
              const Text(
                "Account Data",
                style: TextStyle(
                  fontSize: 25,
                  fontWeight: FontWeight.bold,
                  fontFamily: 'Mulish',
                  color: Colors.white
                ),
              ),
              SizedBox(height: 10),
              Row(children: const [
                Text("Username" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
                SizedBox(width: 48,),
                Text("Pramudiptha" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
              ],),
              SizedBox(height: 10),
              Row(children: const [
                Text("User Type" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
                SizedBox(width: 50,),
                Text("Male" , style: TextStyle(fontFamily: 'Mulish', fontSize: 15, color: Colors.white),),
              ],),
              SizedBox(height: 10),
              OutlineButton(onPressed: () {},
              child: const Text("Change Password", style: TextStyle(fontSize: 15, fontFamily: 'Mulish', fontWeight: FontWeight.bold, color: Colors.white),),
              highlightedBorderColor: const Color(0xE9D8FD),
              shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(6)),
              borderSide: const BorderSide(
                width: 1,
                color: Color.fromRGBO(233, 216, 253, 1),
                style: BorderStyle.solid,
              ),
              )
            ],),
          )
        ],)
      ),
    );
  }
}